const todos = [
  {
    id: 1,
    description: "First todo",
  },
  {
    id: 2,
    description: "Seconds todo",
  },
  {
    id: 3,
    description: "Third todo",
  },
];

const newCourse = {
  id: null,
  title: "",
  authorId: null,
  category: "",
};

// Using CommonJS style export so we can consume via Node (without using Babel-node)
module.exports = {
  newCourse,
  todos,
};
