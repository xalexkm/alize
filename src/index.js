import React from "react";
import { render } from "react-dom";
import App from "./components/App";
import { Provider as ReactProvider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./redux/reducers/index";
import "bootstrap";
import "./styles.scss";

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

render(
  <>
    <ReactProvider store={store}>
      <App />
    </ReactProvider>
  </>,
  document.getElementById("root")
);
