export default function filterReducer(state = [], action) {
  switch (action.type) {
    case "SET_FILTER":
      if (action.filter === null) return [];
      if (
        state.filter((filterInstance) => filterInstance === action.filter)
          .length !== 0
      )
        return [...state.filter((item) => item !== action.filter)];

      return [...state, action.filter];

    default:
      return state;
  }
}
