import initialState from "./initialState";

const postTodoToDb = (todo) => {
  return fetch("http://localhost:3001/todos", {
    method: "POST", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(todo),
  }).catch((err) => console.log(err));
};

const modifyTodoInDb = (todo) => {
  return fetch("http://localhost:3001/todos/" + todo.id.toString(), {
    method: "PUT", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(todo),
  }).catch((err) => console.log(err));
};

const deleteTodoFromDb = (index) => {
  return fetch("http://localhost:3001/todos/" + index.toString(), {
    method: "DELETE",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(),
  });
};

const modifyState = (state, todo) => {
  modifyTodoInDb(todo);
  const [target] = state.filter((target) => todo.id === target.id);
  const index = state.indexOf(target);
  return [...state.slice(0, index).concat(todo), ...state.slice(index + 1)];
};
export default function todosReducer(state = initialState.todos, action) {
  switch (action.type) {
    case "ADD_TODO": {
      postTodoToDb(action.todo);
      return [
        ...state,
        {
          ...action.todo,
          id: state.length > 0 ? state[state.length - 1].id + 1 : 1,
        },
      ];
    }
    case "DELETE_TODO":
      deleteTodoFromDb(action.index);
      return [...state.filter((todo) => todo.id !== action.index)];
    case "MODIFY_TODO":
      return modifyState(state, action.todo);
    case "ADD_FILTER":
      return modifyState(state, action.todo);
    case "FETCH_TODOS":
      return [...state, ...action.todos];
    default:
      return state;
  }
}
