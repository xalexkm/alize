import React from "react";
import "./TodoManager.scss";
import PropTypes from "prop-types";
import TodoColourFilter from "./TodoColourFilter";
import TodoManagerForm from "./common/TodoManagerForm";

function TodoManager({ display, todo, handleSave }) {
  return display ? (
    <>
      <TodoManagerForm handleSave={handleSave} todo={todo} />
      <TodoColourFilter todo={todo} />
    </>
  ) : (
    ""
  );
}

TodoManager.propTypes = {
  display: PropTypes.bool,
  toggleShowManager: PropTypes.func,
  todo: PropTypes.object,
  handleSave: PropTypes.func,
};

export default TodoManager;
