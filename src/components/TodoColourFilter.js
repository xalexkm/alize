import React from "react";
import "./TodoColourFilter.scss";
import { connect } from "react-redux";
import PropTypes from "prop-types";

function TodoColourFilter({ todo, addFilter }) {
  const colours = {
    blue: "#9DCDC0",
    pink: "#F49097",
    orange: "#F18F01",
    yellow: "#F7F052",
  };

  // let colours = new Map([
  //   ["blue", "#9DCDC0"],
  //   ["pink", "#F49097"],
  //   ["orange", "#F18F01"],
  //   ["yellow", "#F7F052"],
  // ]);

  return (
    <div className="row mt-3 mx-auto justify-content-start">
      <div className="col-4">
        <div className="row justify-content-between mx-auto">
          {Object.entries(colours).map(([key, value]) => (
            <button
              key={key}
              className="btn col-2 colour-bit"
              style={{ background: value }}
              onClick={() => addFilter(key, todo)}
            ></button>
          ))}
        </div>
      </div>
    </div>
  );
}

TodoColourFilter.propTypes = {
  addFilter: PropTypes.func,
  todo: PropTypes.object,
};

function mapDispatchToProps(dispatch) {
  return {
    addFilter: (colour, todo) =>
      dispatch({
        type: "ADD_FILTER",
        todo: { description: todo.description, id: todo.id, filter: colour },
      }),
  };
}

export default connect(null, mapDispatchToProps)(TodoColourFilter);
