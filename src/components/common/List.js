import React from "react";
import PropTypes from "prop-types";
import "./List.scss";
import Todo from "../common/Todo";
function List({ todos, handleTodoDelete, handleSave, filter }) {
  return (
    <>
      <div className="col">
        {todos.length > 0 ? (
          todos
            .filter((todo) => {
              if (filter.length === 0 || filter.indexOf(todo.filter) !== -1)
                return todo;
            })
            .map((todo) => (
              <Todo
                key={"item" + todo.id}
                todo={todo}
                handleTodoDelete={handleTodoDelete}
                handleSave={handleSave}
              />
            ))
        ) : (
          <div className="p-3 list--item">There are no todos yet...</div>
        )}
      </div>
    </>
  );
}

List.propTypes = {
  todos: PropTypes.array,
  handleTodoDelete: PropTypes.func,
  handleSave: PropTypes.func,
  fetchTodos: PropTypes.func,
  filter: PropTypes.array,
};

export default List;
