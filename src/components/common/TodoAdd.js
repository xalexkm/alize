import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./TodoAdd.scss";
import plus from "../../assets/plus.png";

let input;
function TodoAdd({ addTodo }) {
  function handleEnter(e) {
    if (e.key === "Enter") {
      addTodo();
      e.target.blur();
      document.removeEventListener("keydown", handleEnter, false);
    }
  }
  return (
    <>
      <div className="col-4">
        <input
          className="form-control font-slim"
          ref={(node) => (input = node)}
          placeholder="Enter todo here..."
          onFocus={() =>
            document.addEventListener("keydown", handleEnter, false)
          }
          onBlur={() =>
            document.removeEventListener("keydown", handleEnter, false)
          }
        />
      </div>
      <div className="col-2">
        <button
          className="btn btn-light form-control"
          onClick={() => addTodo()}
        >
          <img src={plus} alt="plus icon" height="100%" />
        </button>
      </div>
    </>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    addTodo: () => {
      dispatch({
        type: "ADD_TODO",
        todo: {
          description: input.value,
        },
      });
      input.value = "";
    },
  };
}

TodoAdd.propTypes = {
  addTodo: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(TodoAdd);
