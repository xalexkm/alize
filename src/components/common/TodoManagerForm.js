import React from "react";
import PropTypes from "prop-types";

function TodoManagerForm({ handleSave, todo }) {
  let input = todo.description;

  // const handleEnter = (e, input) => {
  //   if (e.key === "Enter") {
  //     handleSave(input, todo);
  //     e.target.blur();
  //     document.removeEventListener("keydown", handleEnter, false);
  //   }
  // };

  return (
    <div className="row mt-3 mx-auto">
      <div className="input-group">
        <div className="input-group-prepend">
          <span className="input-group-text manager-text-label font-slim">
            Title:
          </span>
        </div>

        <input
          type="text"
          id="title"
          className="form-control font-slim"
          defaultValue={input}
          onChange={(e) => (input = e.target.value)}
          // onFocus={(e) =>
          //   document.addEventListener("keydown", handleEnter(e, input), false)
          // }
          // onBlur={(e) =>
          //   document.removeEventListener("keydown", handleEnter(e, input), false)
          // }
        />

        <div className="input-group-append">
          <button
            className="btn btn-outline-light font-slim"
            action="#"
            type="submit"
            onClick={() => handleSave(input, todo)}
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}
TodoManagerForm.propTypes = {
  input: PropTypes.string,
  handleEnter: PropTypes.func,
  todo: PropTypes.object,
  handleSave: PropTypes.func,
};

export default TodoManagerForm;
