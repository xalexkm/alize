import React, { useState } from "react";
import TodoManager from "../TodoManager";
import PropTypes from "prop-types";
import "./Todo.scss";

export default function Todo({ todo, handleTodoDelete, handleSave }) {
  const [showManager, toggleShowManager] = useState(false);
  let colours = new Map([
    ["blue", "#9DCDC0"],
    ["pink", "#F49097"],
    ["orange", "#F18F01"],
    ["yellow", "#F7F052"],
  ]);
  return (
    <>
      <div
        className="row list--item p-3 align-items-center mx-auto"
        key={todo.id}
      >
        <div className="row align-items-center mx-auto">
          <span
            className="col font-slim"
            style={{
              color: "var(--bs-white)",
              whiteSpace: "nowrap",
              overflowX: "hidden",
              textOverflow: "ellipsis",
              textAlign: "start",
            }}
          >
            {todo.description ? todo.description : "Undefined title"}
          </span>
          {todo.filter ? (
            <div
              className="btn col-1 colour-bit"
              style={{
                background: colours.get(todo.filter),
                cursor: "default",
              }}
            ></div>
          ) : (
            <div className="col-2"></div>
          )}
          <div className="btn-group col-4">
            <button
              className="btn btn-outline-light font-slim"
              onClick={() => handleTodoDelete(todo)}
            >
              Delete
            </button>
            <button
              className="btn btn-outline-light font-slim"
              onClick={() => toggleShowManager(!showManager)}
            >
              Manage
            </button>
          </div>
        </div>
        <TodoManager
          key={"manager" + todo.id}
          display={showManager}
          toggleShowManager={() => toggleShowManager(!showManager)}
          todo={todo}
          handleSave={handleSave}
        />
      </div>
    </>
  );
}

Todo.propTypes = {
  todo: PropTypes.object,
  handleTodoDelete: PropTypes.func,
  handleSave: PropTypes.func,
};
