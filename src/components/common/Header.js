import React from "react";
import "./Header.scss";
import TodoAdd from "./TodoAdd";
import PropTypes from "prop-types";
import { connect } from "react-redux";

function Header({ filterByColour }) {
  const colours = {
    blue: "#9DCDC0",
    pink: "#F49097",
    orange: "#F18F01",
    yellow: "#F7F052",
  };
  // let isFiltered = false;
  const toggleFilter = (colour, e) => {
    e.target.style.opacity === "0.5"
      ? (e.target.style.opacity = "1")
      : (e.target.style.opacity = "0.5");
    filterByColour(colour);
    // isFiltered = !isFiltered;
  };
  return (
    <>
      <div className="row form-group justify-content-between p-3 header">
        <TodoAdd />
        <div className="col-4">
          <div className="row justify-content-between mx-auto filter-colour">
            {Object.entries(colours).map(([key, value]) => (
              <button
                className="btn col-2 "
                key={key}
                style={{ background: value, opacity: "0.5" }}
                onClick={(e) => toggleFilter(key, e)}
              ></button>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

Header.propTypes = {
  filterByColour: PropTypes.func,
};
function mapDispatchToProps(dispatch) {
  return {
    filterByColour: (filter) =>
      dispatch({ type: "SET_FILTER", filter: filter }),
  };
}
export default connect(null, mapDispatchToProps)(Header);
