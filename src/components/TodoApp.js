import React, { useEffect } from "react";
import { connect } from "react-redux";
import List from "../components/common/List";
import PropTypes from "prop-types";

function TodoApp({ fetchTodos, todos, handleTodoDelete, handleSave, filter }) {
  useEffect(() => {
    fetch("http://localhost:3001/todos")
      .then((res) => res.json())
      .then(([...data]) => fetchTodos(data));
  }, []);
  return (
    <>
      <div className="row p-3 list">
        {todos ? (
          <List
            // uses todos from state, not props
            todos={todos}
            filter={filter}
            handleTodoDelete={handleTodoDelete}
            handleSave={handleSave}
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
}

function mapStateToProps(state) {
  return {
    todos: state.todos,
    filter: state.filter,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleTodoDelete: (todo) =>
      dispatch({
        type: "DELETE_TODO",
        index: todo.id,
      }),
    handleSave: (input, todo) =>
      dispatch({
        type: "MODIFY_TODO",
        todo: {
          description: input,
          id: todo.id,
          filter: todo.filter,
        },
      }),
    fetchTodos: (todos) =>
      dispatch({
        type: "FETCH_TODOS",
        todos: todos,
      }),
  };
}

// function mapDispatchToProps(dispatch) {
//   return {};
// }

TodoApp.propTypes = {
  todos: PropTypes.array,
  handleTodoDelete: PropTypes.func,
  handleSave: PropTypes.func,
  fetchTodos: PropTypes.func,
  filter: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);
