import React from "react";
import Header from "../components/common/Header";
import TodoApp from "../components/TodoApp";
import "./App.scss";

export default function App() {
  const filterByColour = () => {};
  return (
    <>
      <div className="container-sm mt-5 w-50">
        <div className="row justify-content-center text-center">
          <span className="mb-5 font-normal" style={{ fontSize: "36px" }}>
            ALIZE APP
          </span>

          <Header filterByColour={filterByColour()} />
          <TodoApp />
        </div>
      </div>
    </>
  );
}
